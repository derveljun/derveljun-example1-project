package com.derveljun.exam.home.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.derveljun.exam.base.controller.EJKBaseController;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value="/home")
public class EJKHomeController extends EJKBaseController {
	
	{
		PATH = "home/";
	}
	
	private static final Logger logger = LoggerFactory.getLogger(EJKHomeController.class);
	
}
