package com.derveljun.exam.base.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public class EJKBaseController {
	
	protected String PATH = "";
	protected String PATH_POPUP = PATH + "popup/";
	protected String PATH_INCL = PATH + "include/";
	
    @RequestMapping(value = "/{name:.+}", method = RequestMethod.GET)
    protected String NavigateMenu(@PathVariable("name") String name, HttpServletRequest request, HttpServletResponse response) {
		return this.pageMapper(request, response, PATH + name);
    }
    
	@RequestMapping(value = "/popup/{name:.+}")
    protected String NavigatePopMenu(@PathVariable("name") String name, HttpServletRequest request, HttpServletResponse response) {
		return this.pageMapper(request, response, PATH_POPUP + name);
    }
    
    @RequestMapping(value = "/include/{name:.+}")
    protected String NavigateIncludeMenu(@PathVariable("name") String name, HttpServletRequest request, HttpServletResponse response) {
		return this.pageMapper(request, response, PATH_INCL + name);
    }
    
    private String pageMapper(HttpServletRequest request, HttpServletResponse response, String jspName) {
		return jspName;
	}
}
